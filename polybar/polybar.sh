#!/bin/zsh

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch bar
polybar -c "$USERCONFIG/polybar/main.cfg" main &
polybar -c "$USERCONFIG/polybar/infobar.cfg" info &
