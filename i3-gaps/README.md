# Leader Key
<kbd>Super</kbd>

# i3
<kbd>Mod</kbd>+<kbd>Ctrl</kbd>+<kbd>r</kbd> : Restart i3

# Window Management

## Close Window

<kbd>Mod</kbd>+<kbd>BackSpace</kbd> : Close the window

## Fullscreen

<kbd>Mod</kbd>+<kbd>[</kbd>: Makes the window fullscreen

## Change Focus
<kbd>Mod</kbd>+<kbd>j</kbd> : Focus down

<kbd>Mod</kbd>+<kbd>k</kbd> : Focus up

<kbd>Mod</kbd>+<kbd>l</kbd> : Focus right

<kbd>Mod</kbd>+<kbd>h</kbd> : Focus left

<kbd>Mod</kbd>+<kbd>i</kbd> : Change focus to parent window

<kbd>Mod</kbd>+<kbd>.</kbd> : Change focus between floating and non-floating windows

## Moving to Different Workspaces
<kbd>Mod</kbd>+<kbd>1-0</kbd>                  : Change workspace

<kbd>Mod</kbd>+<kbd>Shift</kbd>+<kbd>1-0</kbd> : Move window to workspace

## Changing the Size
<kbd>Mod</kbd>+<kbd>Shift</kbd>+<kbd>a</kbd> : Increase width by 100px

<kbd>Mod</kbd>+<kbd>Shift</kbd>+<kbd>d</kbd> : Decrease width by 100px

<kbd>Mod</kbd>+<kbd>Shift</kbd>+<kbd>w</kbd> : Increase height by 100px

<kbd>Mod</kbd>+<kbd>Shift</kbd>+<kbd>s</kbd> : Decrease height by 100px

## Floating Windows
<kbd>Mod</kbd>+<kbd>,</kbd> : Toggle floating on window

<kbd>Mod</kbd>+<kbd>Shift</kbd>+<kbd>j</kbd>     : Move up 100

<kbd>Mod</kbd>+<kbd>Shift</kbd>+<kbd>k</kbd>     : Move down 100

<kbd>Mod</kbd>+<kbd>Shift</kbd>+<kbd>l</kbd>     : Move right 100

<kbd>Mod</kbd>+<kbd>Shift</kbd>+<kbd>h</kbd>     : Move left 100

## Window Splits

<kbd>Mod</kbd>+<kbd>Ctrl</kbd>+<kbd>h</kbd> : Split windows in a horizontal layout

<kbd>Mod</kbd>+<kbd>Ctrl</kbd>+<kbd>v</kbd> : Split windows in a vertical layout

## Layouts

<kbd>Mod</kbd>+<kbd>Ctrl</kbd>+<kbd>u</kbd> : Tabbed

<kbd>Mod</kbd>+<kbd>Ctrl</kbd>+<kbd>i</kbd> : Split

## Sticky Windows

<kbd>Mod</kbd>+<kbd>]</kbd> : Makes the window sticky throught the workspaces

# Applications

## Rofi

<kbd>Mod</kbd>+<kbd>d</kbd> : Start rofi in drun mode

<kbd>Mod</kbd>+<kbd>r</kbd> : Start rofi in run mode

<kbd>Mod</kbd>+<kbd>f</kbd> : Start rofi in file finder mode

### Rofi Widgets

<kbd>Mod</kbd>+<kbd>m</kbd>  : Open the music widget

<kbd>Mod</kbd>+<kbd>;</kbd>  : Open keyboard widget

<kbd>Mod</kbd>+<kbd>c</kbd>  : Open the remind widget

<kbd>Mod</kbd>+<kbd>F7</kbd> : Open the power widget

## Misc

<kbd>Mod</kbd>+<kbd>Return</kbd> : Start a new terminal

<kbd>Mod</kbd>+<kbd>u</kbd>      : Open the files manager

<kbd>Mod</kbd>+<kbd>b</kbd>      : Open book library

<kbd>Mod</kbd>+<kbd>e</kbd>      : Open the email software

# Monitor

<kbd>Mod</kbd>+<kbd>F1</kbd> : Only Internal

<kbd>Mod</kbd>+<kbd>F2</kbd> : Only HDMI

<kbd>Mod</kbd>+<kbd>F3</kbd> : Duplicate Internal

<kbd>Mod</kbd>+<kbd>F4</kbd> : Duplicate HDMI

<kbd>Mod</kbd>+<kbd>F5</kbd> : Reset All
