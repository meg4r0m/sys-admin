Some apps I use can be found in my [gentoo-overlay](https://gitlab.com/formigoni/formigoni-overlay).

# Usage

## Global Variables
* The variables $USERCONFIG and $USERSCRIPT were set @ /etc/environment
```shell
USERCONFIG="/home/user/.sys-admin/"
USERSCRIPT="/home/user/.sys-admin/scripts"
```
---

## Configuration files
The configuration files in this repo are for the softwares linked below:
* [i3wm](https://wiki.gentoo.org/wiki/I3)
* [i3lock](https://packages.gentoo.org/packages/x11-misc/i3lock)
* [polybar](https://packages.gentoo.org/packages/x11-misc/polybar)
* [dunst](https://packages.gentoo.org/packages/x11-misc/dunst)
* [rofi](https://packages.gentoo.org/packages/x11-misc/rofi)
* [compton](https://packages.gentoo.org/packages/x11-misc/compton)
* [vim](https://wiki.gentoo.org/wiki/Vim)
---

## Scripts
### Rofi Widgets
![](rofi-widgets.gif)

#### How does it work?
**Important: Rofi opacity must be set to 0 in the compton configuration file.**

**Dependencies:**
* [jq](https://packages.gentoo.org/packages/app-misc/jq): Parsing the *parameters.json* file.
* [setxkbmap](https://packages.gentoo.org/packages/x11-apps/setxkbmap): *kb-layout.sh* script.
* [mpc](https://packages.gentoo.org/packages/media-sound/mpc): *mpc.sh* script.
* [pulseaudio](https://packages.gentoo.org/packages/media-sound/mpc): *pulse-audio.sh* script.
* [remind](https://packages.gentoo.org/packages/app-misc/remind) *reminders.sh* script.
* [transset](https://packages.gentoo.org/packages/x11-apps/transset) this is used by *rofi-effects.sh* to make the rofi window visible.
* [xdotool](https://packages.gentoo.org/packages/x11-misc/xdotool) this is used to move the window around by the *rofi.sh* wrapper script.
* [xwininfo](https://packages.gentoo.org/packages/x11-apps/xwininfo) this is used to get the height of the rofi window *rofi-effects.sh* line 47.


* All the widgets parameters are stored in the *parameters.json* file:
	* Width - The width of the widget.
	* Lines - The maximum number of lines to be displayed.
	* Columns - The number of columns to be displayed.
	* Location - Position in the screen according to the -location parameter as described in the rofi manpage.
	* Prompt - What is written before the : in the widget.
	* Format - (format of the output the -format parameter of rofi's dmenu mode), cmd (the command to generate the output).
	* Cmd - The cmd which will generate the output displayed in the widget.
	* Config - The path to the rofi's configuration file to be used.

* The pop up/down effects are implemented in the *rofi-effects.sh* script:
	* There are four global variables that can be overriden to change the opacity, animation speed, offset from the screen edge (polybarHeight).
	* The rofi\_window\_show() function shows the window immediately without any pop up effect. This is used in the power options widget.
	* The rofi\_window\_pop() moves the window to a non-visible part of the screen, makes it visible and finally slides it into view.

* The *rofi.sh* file is a rofi wrapper that loads all the variables from the parameters from the *parameters.json* file and calls rofi using them. An example of how to use these functions is shown below:

```shell
#!/bin/sh
source "$USERSCRIPT/rofi/rofi.sh"

# Redirect stdout to /dev/null
exec 1>/dev/null
# Redirect stderr to stdout
exec 2>&1

if [[ $(pgrep "rofi") ]]; then # if there's no rofi instance running, exit with an error code
	exit 1
fi

animationSpeed=10 # Override the option defined in rofi-effects.sh

icon=$(echo -e "\uf11c") # Icon from NerdFont

rofi_load_vars keyboard # Load the variables for the keyboard widget defined in the parameters.json file

rofi_dmenu & # Run rofi in dmenu mode using the variables previously loaded (must be in the background)

rofi_window_pop + # Makes the window visible using the 'pop-up' effect

rofi_dmenu_read # Reads the user's choice

# The choice is defined in a variable named 'choice'
if [ "$choice" = "1" ]; then
	setxkbmap -layout us
elif [ "$choice" = "2" ]; then
	setxkbmap -layout br
else
	exit 1
fi
```

This is another example, only this time the user's choice is ignored.
```shell
#!/bin/sh
source "$USERSCRIPT/rofi/rofi.sh"

# Redirect stdout to /dev/null
exec 1>/dev/null
# Redirect stderr to stdout
exec 2>&1

# Override definitions in rofi-effects.sh
animationSpeed=20

if [[ $(pgrep "rofi") ]]; then
	exit 1
fi

rofi_load_vars remind

rofi_dmenu &

rofi_window_pop +
```

### How to put the cover of the album in the rofi window (mpc widget)?
This [manpage](https://manpages.debian.org/testing/rofi/rofi-theme.5.en.html) shows how to configure the box models for rofi. Basically, I've put two boxes side by side, the function *rofi\_dmenu()* defined in the *rofi.sh* script checks if the path to a background was supplied with the function call. Also this special layout requires the usage of a distinguished configuration file, which is *music-background.rasi* file that can be found in the *rofi/* folder on this repo. So the path in the *parameters.json* file would be "\$USERCONFIG/rofi/music-background.rasi".
The script to find the distinguished cover for each song is the same as the widget's, the mpc.sh, which is shown below:

```shell
#!/usr/bin/env bash
source "$USERSCRIPT/rofi-widgets/wrapper/rofi.sh"

# Redirect stdout to /dev/null
#exec 1>/dev/null
# Redirect stderr to stdout
#exec 2>&1

# Override definitions in rofi-effects.sh
animationSpeed=20
maxOpacity=0.95

if [[ $(pgrep "rofi") ]]; then
	exit 1
fi

defaultCover="$USERSCRIPT/rofi-widgets/widgets/default.png"
defaultMusicDir="$HOME/Music"
defaultExtRegex=".+.png|.+.jpg"
defaultCacheName="cover-rofi.png"

# Get current song relative path to defaultMusicDir
file="$(mpc current -f %file%)"
cover="$defaultCover"

# If MPC returned a path to a playing song file
if [[ "$file" ]]; then
	fileDir="$defaultMusicDir/${file%$(basename "$file")}"
	cover="$(find "$fileDir" -type f -name "$defaultCacheName" -print -quit)"

	if [[ ! "$cover" ]]; then
		cover="$(find "$fileDir" -type f -regextype posix-extended -regex "$defaultExtRegex" -print -quit)"
		if [[ "$cover" ]]; then
			convert "$cover" -resize 220x220\! -bordercolor none -border 12x12 "$fileDir/$defaultCacheName"
			cover="$fileDir/$defaultCacheName"
		else
			cover="$defaultCover"
		fi
	fi
fi

rofi_load_vars mpc

rofi_dmenu "$cover" &

rofi_window_pop -

rofi_dmenu_read $!

if [ "$choice" ]; then
	mpc play "$choice"
else
	exit 1
fi
```
---
