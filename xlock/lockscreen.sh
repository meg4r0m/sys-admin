#!/bin/bash

# Set paths and get the current screen resolution
WORKDIR="$HOME/.xlock"
ICON="$WORKDIR/lock.png"
TMPBG="$WORKDIR/lockbg.png"
RES=$(xrandr | awk '/current/ {print $8 "x" $10--}')

# Checks if an argument was passed
# Check if the file exists
# Checks if the workdir exists
if [ $# = 0 ]; then
	echo "No input file."
	exit 1
elif [ ! -e "$1" ]; then
	echo "File does not exist."
	exit 1
elif [ ! -d "$WORKDIR" ]; then
	echo "Directory $WORKDIR does not exist."
	exit 1
fi

# Resize image according to the screen size
# Take a screenshot
# Give an effect
# Merges the icon and the bg
convert "$1" -resize "$RES" "$ICON" || exit 1
scrot "$TMPBG" || exit 1
convert "$TMPBG" -interpolate nearest -virtual-pixel mirror -spread 2 "$TMPBG" || exit 1
convert "$TMPBG" "$ICON" -gravity center -composite "$TMPBG" || exit 1

# Opens up i3lock
i3lock -u -i "$TMPBG" || exit 1
