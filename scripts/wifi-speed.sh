#!/bin/zsh

SSID=$(sudo iw dev | awk '/ssid/ {print substr($0, 8)}')

if [ ! $SSID ]; then
	SSID="Offline"
fi

RI=$(cat /sys/class/net/wlp3s0/statistics/rx_bytes)
TI=$(cat /sys/class/net/wlp3s0/statistics/tx_bytes)
sleep 0.9
RF=$(cat /sys/class/net/wlp3s0/statistics/rx_bytes)
TF=$(cat /sys/class/net/wlp3s0/statistics/tx_bytes)

R=$(expr $(expr $RF - $RI) / 1024)
T=$(expr $(expr $TF - $TI) / 1024)


echo "  ${(r:10:)SSID} 李 ${(r:4:)T} KB/s ${(r:4:)R} KB/s"
