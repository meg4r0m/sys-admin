#! /usr/bin/env python
# Requires "$USERCONFIG/polybar/polybar.sh" file.

import re
import subprocess
import argparse
import time
from functools import partial

xrandr_output = subprocess.check_output(["xrandr"])

# Declare variables
read = False;
screen = None;
resolution = None;
screens = {};

# Get active screens and its resolutions
for line in xrandr_output.decode().split('\n'):
    if read == True:
        resolution = line.split()[0]
        screens.update({screen:resolution})
        read = False
    if re.search(r'\b' + 'connected' + r'\b', line):
        screen = line.split()[0]
        read = True


# Screen Effects
def fade_in(screen):
    for x in range(11):
        subprocess.run(["xrandr", "--output", screen, "--brightness", str(x/10)])
        time.sleep(0.01)

def fade_out(screen):
    for x in range(11):
        subprocess.run(["xrandr", "--output", screen, "--brightness", str((10-x)/10)])
        time.sleep(0.01)

# Turn on and off screens
def turn_off(screen):
    subprocess.run(["xrandr", "--output", screen, "--off"])

def turn_on(screen):
    subprocess.run(["xrandr", "--output", screen, "--auto"])

# Screen management
def single_screen(screen):
    for i in screens:
        fade_out(i)
        turn_off(i)
    
    subprocess.run(["killall", "compton"])
    while subprocess.run(['pgrep compton'], shell=True, check=False).returncode == 0: continue;
    subprocess.run(["killall", "polybar"])
    while subprocess.run(['pgrep polybar'], shell=True, check=False).returncode == 0: continue;

    turn_on(screen)
    subprocess.run(["xrandr", "--output", i, "--rate", "60", "--dpi", "90", "--fb", screens[screen]])
    subprocess.run(["i3-msg", "restart"])
    time.sleep(1)
    subprocess.run(["compton", "-b"])
    subprocess.run(["$USERCONFIG/polybar/polybar.sh &"], shell=True)
    time.sleep(1)
    fade_in(screen)

def dup_screen(screen1, screen2):

    # Fade out and disable screens
    for i in screens:
        fade_out(i)
        turn_off(i)

    # Kill apps
    subprocess.run(["killall", "compton"])
    subprocess.run(["killall", "polybar"])

    # Get coodinated to calculated scales
    x1 = float(screens[screen1].split('x')[0]);
    y1 = float(screens[screen1].split('x')[1]);
    x2 = float(screens[screen2].split('x')[0]);
    y2 = float(screens[screen2].split('x')[1]);

    
    # Enable screens
    for screen in screens:
        turn_on(screen)

    # Scale the second screen to fit the first
    subprocess.run(["xrandr", "--output", screen1, "--fb", screens[screen1], "--scale", "1x1"])
    subprocess.run(["xrandr", "--output", screen2, "--same-as", screen1, "--fb", screens[screen1], "--scale", str(x1/x2)+'x'+str(y1/y2)])

    # Restart apps
    subprocess.run(["i3-msg", "restart"])
    time.sleep(1)
    subprocess.run(["compton", "-b"])
    subprocess.run(["$USERCONFIG/polybar/polybar.sh &"], shell=True)
    time.sleep(1)

    # Fade in screens
    for screen in screens:
        fade_in(screen)

# Create an argument parser
arguments = {
        'only_internal': partial(single_screen, 'eDP-1'),
        'only_hdmi': partial(single_screen, 'HDMI-1'),
        'dup_hdmi': partial(dup_screen, 'HDMI-1', 'eDP-1'),
        'dup_internal': partial(dup_screen, 'eDP-1', 'HDMI-1')
}

parser = argparse.ArgumentParser(description="Script to manage display outputs")
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('--only-hdmi', action="store_true",  help='Only hdmi output')
group.add_argument('--only-internal', action="store_true", help='Only internal output')
group.add_argument('--dup-hdmi', action="store_true", help='Duplicated hdmi output')
group.add_argument('--dup-internal', action="store_true", help='Duplicated internal output')

args = vars(parser.parse_args())

# Execute operation
for arg in args:
    if args[arg] == True:
        arguments[arg]()
