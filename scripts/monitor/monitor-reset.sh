#!/bin/sh

monitors=$(xrandr | awk '/\<connected\>/ {print $1}')

for monitor in $monitors; do
	xrandr --output $monitor --auto --brightness 1
done
