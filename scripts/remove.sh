#!/bin/bash

archivedir="$HOME/.deleted-files/"
realrm="$(which rm)"
copy="$(which cp) -R"

# Let rm return usage error
if [ $# -eq 0 ]; then
	exec $realrm
fi

while getopts "dfiPRrvW" opt; do
	case $opt in
		f ) exec $realrm "$@" ;;
		* ) flags="$flags -$opt" ;;
	esac
done
shift $(( $OPTIND - 1 ))

# Make sure that the $archivedir exists

if [ ! -d $archivedir ]; then
	if [ ! -w $HOME ]; then
		echo "$0 failed: can't create $archivedir in $HOME" >&2
		exit 1
	fi
	mkdir $archivedir
	chmod 700 $archivedir
fi

for arg; do
	newname="$archivedir/$(date "+%d-%m-%y-%Hh%Mm%Ss").$(basename "$arg")"
	if [ -f "$arg" -o -d "$arg" ]; then
		$copy "$arg" "$newname"
	fi
done

exec $realrm $flags "$@"
