#!/bin/sh

OS=$(awk -F "=" '/^NAME=/ {print $2}' /etc/os-release)

echo "  $OS :   $USER"
