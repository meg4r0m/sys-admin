SFOLDER="$HOME/Pictures/Screenshots/"

function screenshot {
	if [ ! -d $SFOLDER ]; then
		mkdir $SFOLDER
		flameshot gui -p $SFOLDER
	else
		flameshot gui -p $SFOLDER
	fi
}

if [ $(pgrep flameshot) ]; then
	screenshot
else
	flameshot &
	sleep 0.5
	screenshot
fi
